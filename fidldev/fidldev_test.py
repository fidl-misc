import itertools
import os
import unittest

from test_util import get_commands, MOCK_FUCHSIA_DIR, MOCK_BUILD_DIR
import regen
import util


# Many mocks contain 3 return values, because the regen script can call
# get_changed_files up to 3 times:
#   1. get the initial set of changed files
#   2. get changed files after fidlc regen, to check if fidlgen needs regen
#   3. get changed files after fidlgen regen, to check if go needs regen
class TestFidlDevRegen(unittest.TestCase):

    def test_basic_regen(self):
        mocks = {
            'get_changed_files':
                itertools.repeat(['zircon/tools/fidl/lib/flat_ast.cc']),
        }
        command = ['regen']
        expected = [['fx', 'regen-goldens', 'fidl']]
        self.assertListEqual(get_commands(mocks, command), expected)

    def test_ir_changed(self):
        mocks = {
            'get_changed_files':
                [
                    ['zircon/tools/fidl/lib/parser.cc'],
                    [
                        'zircon/tools/fidl/lib/parser.cc',
                        'tools/fidl/fidlc/goldens/bits.json.golden'
                    ],
                ]
        }
        command = ['regen']
        expected = [['fx', 'regen-goldens', 'fidl']]
        self.assertListEqual(get_commands(mocks, command), expected)

    def test_tables_changed(self):
        mocks = {
            'get_changed_files':
                [
                    ['zircon/tools/fidl/lib/parser.cc'],
                    [
                        'zircon/tools/fidl/lib/parser.cc',
                        'tools/fidl/fidlc/goldens/bits.tables.c.golden'
                    ],
                ]
        }
        command = ['regen']
        expected = [['fx', 'regen-goldens', 'fidl']]
        self.assertListEqual(get_commands(mocks, command), expected)

    def test_go_goldens_changed(self):
        mocks = {
            'get_changed_files':
                [
                    ['zircon/tools/fidl/lib/parser.cc'],
                    [
                        'zircon/tools/fidl/lib/parser.cc',
                        'tools/fidl/fidlc/goldens/bits.json.golden',
                        'tools/fidl/fidlgen_go/goldens/union.go.golden'
                    ],
                ]
        }
        command = ['regen']
        expected = [
            ['fx', 'regen-goldens', 'fidl'],
            ['fx', 'exec', regen.GO_BINDINGS_REGEN]
        ]
        self.assertListEqual(get_commands(mocks, command), expected)

    def test_fidlgen_go_changed(self):
        mocks = {
            'get_changed_files':
                itertools.repeat(['tools/fidl/fidlgen_go/ir/ir.go'])
        }
        command = ['regen']
        expected = [['fx', 'regen-goldens', 'fidlgen_go']]
        self.assertListEqual(get_commands(mocks, command), expected)

    def test_fidlgen_syzkaller_changed(self):
        mocks = {
            'get_changed_files':
                itertools.repeat(
                    ['tools/fidl/fidlgen_syzkaller/templates/struct.tmpl.go'])
        }
        command = ['regen']
        expected = [['fx', 'regen-goldens', 'fidlgen_syzkaller']]
        self.assertListEqual(get_commands(mocks, command), expected)

    def test_fidldoc_changed(self):
        mocks = {
            'get_changed_files':
                itertools.repeat(
                    ['tools/fidl/fidldoc/src/fidljson.rs'])
        }
        command = ['regen']
        expected = [['fx', 'regen-goldens', 'fidldoc']]
        self.assertListEqual(get_commands(mocks, command), expected)

    def test_fidlgen_dart_changed(self):
        mocks = {
            'get_changed_files':
                itertools.repeat(['tools/fidl/fidlgen_dart/fidlgen_dart.go'])
        }
        command = ['regen']
        expected = [['fx', 'regen-goldens', 'fidlgen_dart']]
        self.assertListEqual(get_commands(mocks, command), expected)

    def test_regen_all(self):
        command = ['regen', 'all']
        expected = [['fx', 'regen-goldens', 'fidl']]
        self.assertListEqual(get_commands({}, command), expected)

    def test_regen_fidlgen(self):
        command = ['regen', 'fidlgen']
        expected = [['fx', 'regen-goldens', 'fidlgen']]
        self.assertListEqual(get_commands({}, command), expected)


class TestFidlDevTest(unittest.TestCase):

    def test_no_changes(self):
        mocks = {'get_changed_files': [[]]}
        command = ['test', '--no-regen']
        self.assertListEqual(get_commands(mocks, command), [])

    def test_fidlc_changed(self):
        mocks = {'get_changed_files': [['zircon/tools/fidl/lib/parser.cc']]}
        command = ['test', '--no-regen']
        expected = [
            util.BUILD_FIDLC_TESTS,
            util.TEST_FIDLC,
            {util.FIDLC_GOLDEN_TEST_TARGET},
        ]
        actual = get_commands(mocks, command)
        self.assertEqual(len(actual), 3)
        self.assertEqual(actual[0], expected[0])
        self.assertEqual(actual[1], expected[1])
        self.assertTestsRun(actual[2], expected[2])

    def test_ir_changed_zircon(self):
        mocks = {
            'get_changed_files':
                [['tools/fidl/fidlc/goldens/bits.json.golden']]
        }
        command = ['test', '--no-regen']
        expected = [
            util.BUILD_FIDLC_TESTS,
            util.TEST_FIDLC,
            {util.FIDLC_GOLDEN_TEST_TARGET},
        ]
        actual = get_commands(mocks, command)
        self.assertEqual(len(actual), 3)
        self.assertEqual(actual[0], expected[0])
        self.assertEqual(actual[1], expected[1])
        self.assertTestsRun(actual[2], expected[2])

    def test_fidlgen_util_changed(self):
        mocks = {
            'get_changed_files':
                [[
                    'tools/fidl/lib/fidlgen/types/types.go',
                ]]
        }
        command = ['test', '--no-regen']
        actual = get_commands(mocks, command)
        expected = set(util.FIDLGEN_TEST_TARGETS)
        self.assertEqual(len(actual), 1)
        self.assertTestsRun(actual[0], expected)

    def test_fidlgen_backend_changed(self):
        mocks = {
            'get_changed_files':
                [[
                    'tools/fidl/fidlgen_rust/templates/enum.tmpl.go',
                ]]
        }
        command = ['test', '--no-regen']
        actual = get_commands(mocks, command)
        expected = set(util.FIDLGEN_TEST_TARGETS)
        self.assertEqual(len(actual), 1)
        self.assertTestsRun(actual[0], expected)

    def test_fidlgen_golden_changed(self):
        mocks = {
            'get_changed_files':
                [
                    [
                        'tools/fidl/fidlgen_hlcpp/goldens/union.cc.golden',
                    ]
                ]
        }
        command = ['test', '--no-regen']
        actual = get_commands(mocks, command)
        expected = set(util.FIDLGEN_TEST_TARGETS) | {util.HLCPP_TEST_TARGET}
        self.assertEqual(len(actual), 1)
        self.assertTestsRun(actual[0], expected)

    def test_c_runtime_changed(self):
        mocks = {
            'get_changed_files': [[
                'zircon/system/ulib/fidl/txn_header.c',
            ]]
        }
        command = ['test', '--no-regen']
        actual = get_commands(mocks, command)
        expected = {
            util.HLCPP_TEST_TARGET,
            util.LLCPP_TEST_TARGET,
            util.C_TEST_TARGET,
        }
        self.assertEqual(len(actual), 1)
        self.assertTestsRun(actual[0], expected)

    def test_coding_tables_changed(self):
        mocks = {
            'get_changed_files':
                [[
                    'tools/fidl/fidlc/goldens/union.tables.c.golden',
                ]]
        }
        command = ['test', '--no-regen']
        actual = get_commands(mocks, command)
        expected = {
            util.HLCPP_TEST_TARGET,
            util.LLCPP_TEST_TARGET,
            util.C_TEST_TARGET,
            util.FIDLC_GOLDEN_TEST_TARGET,
        }
        self.assertEqual(actual[1], util.TEST_FIDLC)
        self.assertTestsRun(actual[2], expected)

    def test_go_runtime_changed(self):
        mocks = {
            'get_changed_files':
                [[
                    'third_party/go/src/syscall/zx/fidl/encoding_new.go',
                ]]
        }
        command = ['test', '--no-regen']
        actual = get_commands(mocks, command)
        expected = {util.GO_CONFORMANCE_TEST_TARGET}.union(set(util.GO_TEST_TARGETS))
        self.assertEqual(len(actual), 1)
        self.assertTestsRun(actual[0], expected)

    def test_dart_runtime_changed(self):
        mocks = {
            'get_changed_files':
                [
                    [
                        'sdk/dart/fidl/lib/src/types.dart',
                        'sdk/dart/fidl/lib/src/message.dart',
                    ]
                ]
        }
        command = ['test', '--no-regen']
        actual = get_commands(mocks, command)
        expected = {util.DART_TEST_TARGET}
        self.assertEqual(len(actual), 1)
        self.assertTestsRun(actual[0], expected)

    def test_gidl_changed(self):
        mocks = {
            'get_changed_files':
                [
                    [
                        'tools/fidl/gidl/main.go ',
                        'tools/fidl/gidl/rust/benchmarks.go',
                        'tools/fidl/gidl/rust/conformance.go',
                    ]
                ]
        }
        command = ['test', '--no-regen']
        actual = get_commands(mocks, command)
        expected = {
            util.GIDL_TEST_TARGET,
            util.GO_CONFORMANCE_TEST_TARGET,
            util.HLCPP_CONFORMANCE_TEST_TARGET,
            util.HLCPP_HOST_CONFORMANCE_TEST_TARGET,
            util.LLCPP_CONFORMANCE_TEST_TARGET,
            util.RUST_CONFORMANCE_TEST_TARGET,
            util.DART_TEST_TARGET,
        }
        self.assertEqual(len(actual), 1)
        self.assertTestsRun(actual[0], expected)

    def test_extra_flags(self):
        mocks = {
            'get_changed_files': [[
                'zircon/tools/fidl/lib/parser.cc',
                'sdk/dart/fidl/lib/src/types.dart',
            ]]
        }
        command = [
            'test', '--no-regen', '--fx-test-args=-ov',
            '--gtest_filter=\'EnumsTests.*\''
        ]
        actual = get_commands(mocks, command)

        self.assertEqual(len(actual), 3)
        self.assertEqual(actual[0], util.BUILD_FIDLC_TESTS)
        self.assertEqual(
            actual[1],
            'fuchsia_dir/out/default/host_x64/fidl-compiler --gtest_filter=\'EnumsTests.*\''
        )
        self.assertEqual(actual[2][0], 'fx')
        self.assertEqual(actual[2][1], 'test')
        self.assertEqual(actual[2][2], '-ov')
        self.assertEqual(actual[2][3], util.DART_TEST_TARGET)
        self.assertEqual(actual[2][4], util.FIDLC_GOLDEN_TEST_TARGET)

    def assertTestsRun(self, raw_command, expected):
        self.assertEqual(raw_command[0], 'fx')
        self.assertEqual(raw_command[1], 'test')
        self.assertEqual(raw_command[2], '-v')
        tests = set(raw_command[3:])
        self.assertSetEqual(tests, expected)


if __name__ == '__main__':
    unittest.main()
