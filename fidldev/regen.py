import os

import util


GO_BINDINGS_REGEN = os.path.join(util.FUCHSIA_DIR, 'third_party/go/regen-fidl')


def is_go_bindings_changed():
    goldens_dir = util.goldens_dir('fidlgen_go')
    for path in util.get_changed_files():
        if path.startswith(goldens_dir):
            return True
    return False


REGEN_TARGETS = [
    'fidlc',
    'fidldoc',
    'fidlgen',
    'go',
    *util.FIDLGEN_BACKEND_DIRS.keys(),
]


def regen_explicit(targets, dry_run):
    regen = set()
    regen_go = False
    for target in targets:
        if target == 'all':
            regen.add('fidl')
        elif target == 'go':
            regen_go = True
        else:
            regen.add(target)
    util.run(['fx', 'regen-goldens', *regen], dry_run, exit_on_failure=True)
    if regen_go:
        util.run(['fx', 'exec', GO_BINDINGS_REGEN], dry_run, exit_on_failure=True)


def regen_changed(changed_files, dry_run):
    regen = set()
    for file_ in changed_files:
        if file_.startswith(util.FIDLC_DIR):
            # If fidlc goldens change, we need to regenerate goldens for all
            # other tools. Rather than checking to see if they actually change,
            # it's faster to just regenerate everything. So, we add 'fidl' to
            # match all FIDL tools, rather than just 'fidlc'.
            regen.add('fidl')
        if file_.startswith(util.FIDLDOC_DIR):
            regen.add('fidldoc')
        if file_.startswith(util.FIDLGEN_LIB_DIR):
            regen.update(util.FIDLGEN_BACKEND_DIRS.keys())
        if file_.startswith(util.FIDLGEN_CPP_LIB_DIR):
            regen.add('fidlgen_hlcpp')
            regen.add('fidlgen_llcpp')
        for backend, path in util.FIDLGEN_BACKEND_DIRS.items():
            if file_.startswith(path):
                regen.add(backend)

    if regen:
        util.run(['fx', 'regen-goldens', *regen], dry_run, exit_on_failure=True)

    if ("fidl" in regen or "fidlgen_go" in regen) and (dry_run or is_go_bindings_changed()):
        util.run(['fx', 'exec', GO_BINDINGS_REGEN], dry_run, exit_on_failure=True)
