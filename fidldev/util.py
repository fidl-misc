import os
import shlex
import subprocess

from env import FUCHSIA_DIR, BUILD_DIR, PLATFORM

FIDL_TESTDATA_DIR = 'zircon/tools/fidl/testdata'

FIDLC_DIR = 'zircon/tools/fidl'
FIDLDOC_DIR = 'tools/fidl/fidldoc'
FIDLGEN_LIB_DIR = 'tools/fidl/lib/fidlgen'
FIDLGEN_CPP_LIB_DIR = 'tools/fidl/lib/fidlgen_cpp'
FIDLGEN_BACKEND_DIRS = {
    'fidlgen_dart': 'tools/fidl/fidlgen_dart',
    'fidlgen_go': 'tools/fidl/fidlgen_go',
    'fidlgen_hlcpp': 'tools/fidl/fidlgen_hlcpp',
    'fidlgen_libfuzzer': 'tools/fidl/fidlgen_libfuzzer',
    'fidlgen_llcpp': 'tools/fidl/fidlgen_llcpp',
    'fidlgen_rust': 'tools/fidl/fidlgen_rust',
    'fidlgen_syzkaller': 'tools/fidl/fidlgen_syzkaller',
}
ALL_FIDLGEN_DIRS = [
    FIDLGEN_LIB_DIR,
    FIDLGEN_CPP_LIB_DIR,
    *FIDLGEN_BACKEND_DIRS.values()
]

TEST_FIDLC = os.path.join(BUILD_DIR, 'host_x64/fidl-compiler')
FIDLC_GOLDEN_TEST_TARGET = '//tools/fidl/fidlc'
FIDLGEN_TEST_TARGETS = ['//tools/fidl/' + path for path in FIDLGEN_BACKEND_DIRS.keys()]
HLCPP_TEST_TARGET = '//sdk/lib/fidl'
LLCPP_TEST_TARGET = '//src/lib/fidl/llcpp'
C_TEST_TARGET = '//src/lib/fidl/c'
# these tests can't be consolidated under a GN label, because all third_party/go
# tests are located under //third_party/go
GO_TEST_TARGETS = [
    'go-fidl-tests',
    'go_extended_fidl_test',
    'go_unsafevalue_test',
]
RUST_TEST_TARGET = '//src/lib/fidl/rust'
DART_TEST_TARGET = '//src/tests/fidl/dart_bindings_test'
GIDL_TEST_TARGET = '//tools/fidl/gidl'

HLCPP_CONFORMANCE_TEST_TARGET = 'fidl_hlcpp_conformance_tests'
LLCPP_CONFORMANCE_TEST_TARGET = 'fidl_llcpp_conformance_tests'
GO_CONFORMANCE_TEST_TARGET = 'fidl_go_conformance_tests'
RUST_CONFORMANCE_TEST_TARGET = 'fidl_rust_conformance_tests'

HLCPP_RUNTIME = 'sdk/lib/fidl'
LLCPP_RUNTIME = 'src/lib/fidl/llcpp'
C_RUNTIME = 'zircon/system/ulib/fidl'
GO_RUNTIME = 'third_party/go/src/syscall/zx/fidl'
RUST_RUNTIME = 'src/lib/fidl/rust'
DART_RUNTIME = 'sdk/dart/fidl'

BUILD_FIDLC = ['fx', 'ninja', '-C', BUILD_DIR, 'host_x64/fidlc']
BUILD_FIDLC_TESTS = ['fx', 'ninja', '-C', BUILD_DIR, 'host_x64/fidl-compiler']
BUILD_FIDLGEN = ['fx', 'build', 'tools/fidl']
BUILD_FIDLGEN_DART = ['fx', 'ninja', '-C', BUILD_DIR, 'host_x64/fidlgen_dart']


def goldens_dir(tool):
    if tool == 'fidlc':
        return 'tools/fidl/fidlc/goldens'
    if tool == 'fidldoc':
        return 'tools/fidl/fidldoc/goldens'
    return FIDLGEN_BACKEND_DIRS[tool] + '/goldens'


def run(command, dry_run, exit_on_failure=False):
    """
    Run the given command, returning True if it completed successfuly. If
    dry_run is true, just prints rather than running. If exit_on_failure is
    true, exits instead of returning False.
    WARNING: passing a str command will use shell=True
    """
    is_str = isinstance(command, str)
    if dry_run:
        print('would run: {}'.format(command if is_str else shlex.join(command)))
        return True
    retcode = subprocess.call(command, shell=is_str)
    success = retcode == 0
    if exit_on_failure and not success:
        print_err('Error: command failed with status {}! {}'.format(
            retcode, command))
        exit(1)
    return success


def get_changed_files():
    """
    Return a List of paths relative to FUCHSIA_DIR of changed files relative to
    the parent. This uses the same logic as fx format-code.
    """
    upstream = "origin/master"
    local_commit = subprocess.check_output(
        "git rev-list HEAD ^{} -- 2>/dev/null | tail -1".format(upstream),
        shell=True).strip().decode()
    diff_base = subprocess.check_output(
        ['git', 'rev-parse', local_commit +
         '^']).strip().decode() if local_commit else "HEAD"
    files = subprocess.check_output(['git', 'diff', '--name-only',
                                     diff_base]).strip().decode().split('\n')

    repo = subprocess.check_output(['git', 'rev-parse',
                                    '--show-toplevel']).strip().decode()
    # add prefixes so that all and targets can be specified relative to FUCHSIA_DIR
    if repo.endswith('topaz'):
        files = [os.path.join('topaz', p) for p in files]
    elif repo.endswith('third_party/go'):
        files = [os.path.join('third_party/go', p) for p in files]

    return files


RED = '\033[1;31m'
YELLOW = '\033[1;33m'
NC = '\033[0m'


def print_err(s):
    print_color(s, RED)


def print_warning(s):
    print_color(s, YELLOW)


def print_color(s, color):
    print('{}{}{}'.format(color, s, NC))
