# fidl-lsp

`fidl-lsp` is an [LSP](https://microsoft.github.io/language-server-protocol/)
Language Server for [FIDL](https://fuchsia.dev/fuchsia-src/development/languages/fidl).

## Getting Started

See the [VSCode extension README][vscode-extension-readme] for more information
on how to install and setup the FIDL LSP extension in VSCode.

## Contributing

To launch a local test instance of VSCode running the extension:

1. Follow the [Fuchsia: Get Started](https://fuchsia.dev/fuchsia-src/getting_started)
   guide.
2. Run `fx gen` and then `fx build build/fidl:validate_fidl_project_json` to
   make sure fidl_project.json exists.
3. Build `fidl-lint`: `fx build host_x64/fidl-lint`.
4. Ensure that you have [Go](https://golang.org/) and [Node.js](https://nodejs.org/)
   installed.
5. Fetch prebuilts and compile the language server and VSCode extension by
   running `build-vscode.sh`.
6. Open the `fidl-misc/vscode-language-fidl/` directory in VSCode, and run the
   `Launch Extension` task.

## Testing

To run tests: `go test -p 1 ./...`

The `-p` flag specifies that we don't want tests run in parallel, as some of
them write to and read from the filesystem and can collide. If you want to force
your tests to be re-run (as test results are cached and not re-run until code in
that package has changed), you can run with the flag `-count 1`.

## Architecture

The general flow of control in the language server goes like this:
* A request/notification is received in `langserver/handler.go`
* If it is a notification (changing state of the open files):
  * The `LangHandler` tells the `FileSystem` to make the change
  * The `LangHandler` triggers a re-analysis by the `Analyzer`
* If it is a request for some language feature:
  * The `LangHandler` dispatches the request to the `Analyzer`
  * The `Analyzer` extracts the needed information and returns it
  * The `LangHandler` sends the response to the client

### `langserver` package

This package includes the core logic of the language server. It deals with LSP
specific boilerplate, JSON-RPC messages, etc. It includes the `LangHandler`
type, which handles LSP requests, sends reponses and notifications to the
client, and dispatches changes to the state manager or requests for analyses to
the `Analyzer`.

### `state` package

The language server's state management is in the `state` package. Currently this
is just an in memory file system (mapping of editor file names to file text).
This could be wrapped in e.g. an `RWLock` to enable concurrent handling of LSP
requests and notifications.

### `analysis` package

The "backend" of the server, which doesn't know about LSP but knows how
to compile FIDL and analyze the JSON IR.

Includes the `Analyzer` type, which maintains a set of compiled FIDL `Library`s
and their constituent files, dependencies, and build artifacts. Main entry point
is `Analyzer.Analyze()`, which is called every time a file is changed on the
client. `Analyze` recompiles the relevant FIDL library for that file and imports
the JSON IR.

The `Analyzer` compiles FIDL libraries by invoking `fidlc` in a separate
process, gets diagnostics from `fidlc` and `fidl-lint`, and uses `fidl-format`
for formatting. It locates dependencies using a `fidl_project.json` file, the
path to which will be configurable in the LSP client extension. `fidl_project.json`
declares all FIDL libraries the language server should be aware of, the paths to
their constituent files, the path to their JSON IR, and their dependencies (by
library name).

## Supported LSP features

This list documents LSP features supported by fidl-lsp.

### General
- [x] [initialize](https://microsoft.github.io/language-server-protocol/specification#initialize)
- [x] [initialized](https://microsoft.github.io/language-server-protocol/specification#initialized)
- [x] [shutdown](https://microsoft.github.io/language-server-protocol/specification#shutdown)
- [x] [exit](https://microsoft.github.io/language-server-protocol/specification#exit)
- [ ] [$/cancelRequest](https://microsoft.github.io/language-server-protocol/specification#cancelRequest)

### Workspace
- [ ] [workspace/workspaceFolders](https://microsoft.github.io/language-server-protocol/specification#workspace_workspaceFolders)
- [ ] [workspace/didChangeWorkspaceFolders](https://microsoft.github.io/language-server-protocol/specification#workspace_didChangeWorkspaceFolders)
- [x] [workspace/didChangeConfiguration](https://microsoft.github.io/language-server-protocol/specification#workspace_didChangeConfiguration)
- [x] [workspace/configuration](https://microsoft.github.io/language-server-protocol/specification#workspace_configuration)
- [ ] [workspace/didChangeWatchedFiles](https://microsoft.github.io/language-server-protocol/specification#workspace_didChangeWatchedFiles)
- [ ] [workspace/symbol](https://microsoft.github.io/language-server-protocol/specification#workspace_symbol)
- [ ] [workspace/applyEdit](https://microsoft.github.io/language-server-protocol/specification#workspace_applyEdit)

### Text Synchronization
- [x] [textDocument/didOpen](https://microsoft.github.io/language-server-protocol/specification#textDocument_didOpen)
- [x] [textDocument/didChange](https://microsoft.github.io/language-server-protocol/specification#textDocument_didChange)
- [ ] [textDocument/willSave](https://microsoft.github.io/language-server-protocol/specification#textDocument_willSave)
- [ ] [textDocument/willSaveWaitUntil](https://microsoft.github.io/language-server-protocol/specification#textDocument_willSaveWaitUntil)
- [ ] [textDocument/didSave](https://microsoft.github.io/language-server-protocol/specification#textDocument_didSave)
- [ ] [textDocument/didClose](https://microsoft.github.io/language-server-protocol/specification#textDocument_didClose)

### Diagnostics
- [x] [textDocument/publishDiagnostics](https://microsoft.github.io/language-server-protocol/specification#textDocument_publishDiagnostics)

### Language Features
- [ ] [textDocument/completion](https://microsoft.github.io/language-server-protocol/specification#textDocument_completion)
- [ ] [completionItem/resolve](https://microsoft.github.io/language-server-protocol/specification#completionItem_resolve)
- [x] [textDocument/hover](https://microsoft.github.io/language-server-protocol/specification#textDocument_hover)
- [ ] [textDocument/signatureHelp](https://microsoft.github.io/language-server-protocol/specification#textDocument_signatureHelp)
- [ ] [textDocument/declaration](https://microsoft.github.io/language-server-protocol/specification#textDocument_declaration)
- [x] [textDocument/definition](https://microsoft.github.io/language-server-protocol/specification#textDocument_definition)
- [ ] [textDocument/typeDefinition](https://microsoft.github.io/language-server-protocol/specification#textDocument_typeDefinition)
- [ ] [textDocument/implementation](https://microsoft.github.io/language-server-protocol/specification#textDocument_implementation)
- [x] [textDocument/references](https://microsoft.github.io/language-server-protocol/specification#textDocument_references)
- [ ] [textDocument/documentHighlight](https://microsoft.github.io/language-server-protocol/specification#textDocument_documentHighlight)
- [ ] [textDocument/documentSymbol](https://microsoft.github.io/language-server-protocol/specification#textDocument_documentSymbol)
- [ ] [textDocument/codeAction](https://microsoft.github.io/language-server-protocol/specification#textDocument_codeAction)
- [ ] [textDocument/selectionRange](https://github.com/Microsoft/language-server-protocol/issues/613)
- [ ] [textDocument/codeLens](https://microsoft.github.io/language-server-protocol/specification#textDocument_codeLens)
- [ ] [codeLens/resolve](https://microsoft.github.io/language-server-protocol/specification#codeLens_resolve)
- [x] [textDocument/documentLink](https://microsoft.github.io/language-server-protocol/specification#textDocument_documentLink)
- [ ] [documentLink/resolve](https://microsoft.github.io/language-server-protocol/specification#documentLink_resolve)
- [ ] [textDocument/documentColor](https://microsoft.github.io/language-server-protocol/specification#textDocument_documentColor)
- [ ] [textDocument/colorPresentation](https://microsoft.github.io/language-server-protocol/specification#textDocument_colorPresentation)
- [x] [textDocument/formatting](https://microsoft.github.io/language-server-protocol/specification#textDocument_formatting)
- [ ] [textDocument/rangeFormatting](https://microsoft.github.io/language-server-protocol/specification#textDocument_rangeFormatting)
- [ ] [textDocument/onTypeFormatting](https://microsoft.github.io/language-server-protocol/specification#textDocument_onTypeFormatting)
- [ ] [textDocument/rename](https://microsoft.github.io/language-server-protocol/specification#textDocument_rename)
- [ ] [textDocument/prepareRename](https://microsoft.github.io/language-server-protocol/specification#textDocument_prepareRename)
- [ ] [textDocument/foldingRange](https://microsoft.github.io/language-server-protocol/specification#textDocument_foldingRange)

[vscode-extension-readme]: https://fuchsia.googlesource.com/fidl-misc/+/refs/heads/master/vscode-language-fidl/README.md
