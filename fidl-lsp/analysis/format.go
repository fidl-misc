// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package analysis

import (
	"fmt"
	"os/exec"

	"fidl-lsp/state"
)

// FormatFile runs fidl-format on the file at `path` in the FileSystem, and
// returns the formatted document, or an error.
func (a *Analyzer) FormatFile(fs *state.FileSystem, path state.FileID) (string, error) {
	// TODO: write tmp file on all changes in langserver?
	analysisInputFile, err := a.writeFileToTmp(fs, path)
	if err != nil {
		return "", fmt.Errorf("error writing to tmp file: %s", err)
	}

	out, err := exec.Command(a.cfg.FidlFormatPath, analysisInputFile).Output()
	if err != nil {
		return "", fmt.Errorf("error running fidl-format: %s", err)
	}
	return string(out), nil
}
