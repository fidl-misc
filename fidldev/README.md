# fidldev

`fidldev` is a FIDL development workflow tool. Its goal is to automate
repetitive processes while working on FIDL code, like running tests based on
changed files, and regenerating golden files. It is also meant to be the
source of truth for FIDL code locations and tests/regen commands.

## Running fidldev:

    $FIDLMISC_DIR/fidldev/fidldev.py --help

This can be aliased for convenienced:

    alias fidldev=$FIDLMISC_DIR/fidldev/fidldev.py

Note that `fidldev` should be run from a valid repo (e.g. fuchsia.git, topaz,
or third_party/go) in order to correctly detect changes, similar to
`fx format-code`.

## Testing fidldev:

    python3 $FIDLMISC_DIR/fidldev/fidldev_test.py -b

The `-b` flag will separate stdout output when printing test results. It can
be removed when using debugging print statements in the test itself.
