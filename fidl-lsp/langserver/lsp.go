// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package langserver

import "github.com/sourcegraph/go-lsp"

// `go-lsp` does not include all the necessary types for an LSP server. For
// example, the types associated with the "textDocument/documentLink" language
// feature are missing. This file includes those missing definitions.

type serverCapabilities struct {
	TextDocumentSync                 *lsp.TextDocumentSyncOptionsOrKind   `json:"textDocumentSync,omitempty"`
	HoverProvider                    bool                                 `json:"hoverProvider,omitempty"`
	CompletionProvider               *lsp.CompletionOptions               `json:"completionProvider,omitempty"`
	SignatureHelpProvider            *lsp.SignatureHelpOptions            `json:"signatureHelpProvider,omitempty"`
	DefinitionProvider               bool                                 `json:"definitionProvider,omitempty"`
	TypeDefinitionProvider           bool                                 `json:"typeDefinitionProvider,omitempty"`
	ReferencesProvider               bool                                 `json:"referencesProvider,omitempty"`
	DocumentHighlightProvider        bool                                 `json:"documentHighlightProvider,omitempty"`
	DocumentSymbolProvider           bool                                 `json:"documentSymbolProvider,omitempty"`
	WorkspaceSymbolProvider          bool                                 `json:"workspaceSymbolProvider,omitempty"`
	ImplementationProvider           bool                                 `json:"implementationProvider,omitempty"`
	CodeActionProvider               bool                                 `json:"codeActionProvider,omitempty"`
	CodeLensProvider                 *lsp.CodeLensOptions                 `json:"codeLensProvider,omitempty"`
	DocumentFormattingProvider       bool                                 `json:"documentFormattingProvider,omitempty"`
	DocumentRangeFormattingProvider  bool                                 `json:"documentRangeFormattingProvider,omitempty"`
	DocumentOnTypeFormattingProvider *lsp.DocumentOnTypeFormattingOptions `json:"documentOnTypeFormattingProvider,omitempty"`
	DocumentLinkProvider             *documentLinkOptions                 `json:"documentLinkProvider,omitempty"`
	RenameProvider                   bool                                 `json:"renameProvider,omitempty"`
	ExecuteCommandProvider           *lsp.ExecuteCommandOptions           `json:"executeCommandProvider,omitempty"`
	SemanticHighlighting             *lsp.SemanticHighlightingOptions     `json:"semanticHighlighting,omitempty"`
	Experimental                     interface{}                          `json:"experimental,omitempty"`
}

type initializeResult struct {
	Capabilities serverCapabilities `json:"capabilities,omitempty"`
}

type documentLinkOptions struct {
	ResolveProvider bool `json:"resolveProvider,omitempty"`
}

type documentLinkParams struct {
	TextDocument lsp.TextDocumentIdentifier `json:"textDocument"`
}

type documentLink struct {
	Range   lsp.Range       `json:"range"`
	Target  lsp.DocumentURI `json:"target"`
	Tooltip string          `json:"tooltip"`
}
