### Example Use

    $ swipl
    ['fidl.pl'].
    phrase(encode(struct([b1, b4])), E).

### Links

* [SWI Prolog][swiprolog]
* [prolog :- tutorial][prolog_tutorial]
* [Definite Clause Grammars][dcg_tutorial]
* [A Prologue for Prolog][prologue_for_prolog]

<!-- xrefs -->
[swiprolog]: http://www.swi-prolog.org
[prolog_tutorial]: https://www.cpp.edu/~jrfisher/www/prolog_tutorial/contents.html
[dcg_tutorial]: http://www.pathwayslms.com/swipltuts/dcg/
[prologue_for_prolog]: http://www.complang.tuwien.ac.at/ulrich/iso-prolog/prologue
