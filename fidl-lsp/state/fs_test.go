// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package state_test

import (
	"testing"

	"fidl-lsp/state"
)

func TestFsApplyChangesInsertions(t *testing.T) {
	fs := state.NewFileSystem()

	fs.NewFile("test.fidl", `
library example;

struct Foo {
    uint8 foo;
};
`)

	err := fs.ApplyChanges("test.fidl",
		[]state.Change{
			{
				Range: state.Range{
					Start: state.Position{Line: 1, Character: 10},
					End:   state.Position{Line: 1, Character: 10},
				},
				NewContent: "tended.library.ex",
			},
			{
				Range: state.Range{
					Start: state.Position{Line: 2, Character: 0},
					End:   state.Position{Line: 2, Character: 0},
				},
				NewContent: "\nconst uint8 INSERTED_CONST = 0;\n",
			},
			{
				Range: state.Range{
					Start: state.Position{Line: 5, Character: 0},
					End:   state.Position{Line: 5, Character: 0},
				},
				NewContent: "/// Inserted doc comment\n",
			},
			{
				Range: state.Range{
					Start: state.Position{Line: 7, Character: 14},
					End:   state.Position{Line: 7, Character: 14},
				},
				NewContent: "\n    string inserted_struct_member;",
			},
		},
	)
	if err != nil {
		t.Errorf("error on applying changes: %s", err)
	}

	expFile := `
library extended.library.example;

const uint8 INSERTED_CONST = 0;

/// Inserted doc comment
struct Foo {
    uint8 foo;
    string inserted_struct_member;
};
`
	file, err := fs.File("test.fidl")
	if err != nil {
		t.Error(err)
	}
	if file != expFile {
		t.Errorf(
			"file changes were not applied correctly. expected:\n%s\nfound:\n%s\n",
			expFile,
			file,
		)
	}
}

func TestFsApplyChangesDeletions(t *testing.T) {
	fs := state.NewFileSystem()

	fs.NewFile("test.fidl", `
library example;

struct ToBeDeleted {
    uint8 foo;
};
`)

	err := fs.ApplyChanges("test.fidl",
		[]state.Change{
			{
				Range: state.Range{
					Start: state.Position{Line: 3, Character: 0},
					End:   state.Position{Line: 6, Character: 0},
				},
				NewContent: "",
			},
		},
	)
	if err != nil {
		t.Errorf("error on applying changes: %s", err)
	}

	expFile := `
library example;

`
	file, err := fs.File("test.fidl")
	if err != nil {
		t.Error(err)
	}
	if file != expFile {
		t.Errorf(
			"file changes were not applied correctly. expected:\n%s\nfound:\n%s\n",
			expFile,
			file,
		)
	}
}

func TestFsApplyChangesToBeginningOfFile(t *testing.T) {
	fs := state.NewFileSystem()

	fs.NewFile(
		"test.fidl",
		`library example;`,
	)

	err := fs.ApplyChanges("test.fidl",
		[]state.Change{
			{
				Range: state.Range{
					Start: state.Position{Line: 0, Character: 0},
					End:   state.Position{Line: 0, Character: 0},
				},
				NewContent: "// Prepended comment\n",
			},
		},
	)
	if err != nil {
		t.Errorf("error on applying changes: %s", err)
	}

	expFile := `// Prepended comment
library example;`
	file, err := fs.File("test.fidl")
	if err != nil {
		t.Error(err)
	}
	if file != expFile {
		t.Errorf(
			"file changes were not applied correctly. expected:\n%s\nfound:\n%s\n",
			expFile,
			file,
		)
	}

	err = fs.ApplyChanges("test.fidl",
		[]state.Change{
			{
				Range: state.Range{
					Start: state.Position{Line: 0, Character: 0},
					End:   state.Position{Line: 1, Character: 0},
				},
				NewContent: "",
			},
		},
	)
	if err != nil {
		t.Errorf("error on applying changes: %s", err)
	}

	expFile = `library example;`
	file, err = fs.File("test.fidl")
	if err != nil {
		t.Error(err)
	}
	if file != expFile {
		t.Errorf(
			"file changes were not applied correctly. expected:\n%s\nfound:\n%s\n",
			expFile,
			file,
		)
	}
}

func TestFsApplyChangesToEndOfFile(t *testing.T) {
	fs := state.NewFileSystem()

	fs.NewFile("test.fidl", `
library example;
`)

	err := fs.ApplyChanges("test.fidl",
		[]state.Change{
			{
				Range: state.Range{
					Start: state.Position{Line: 1, Character: 16},
					End:   state.Position{Line: 1, Character: 16},
				},
				NewContent: "\n// Appended comment",
			},
		},
	)
	if err != nil {
		t.Errorf("error on applying changes: %s", err)
	}

	expFile := `
library example;
// Appended comment
`
	file, err := fs.File("test.fidl")
	if err != nil {
		t.Error(err)
	}
	if file != expFile {
		t.Errorf(
			"file changes were not applied correctly. expected:\n%s\nfound:\n%s\n",
			expFile,
			file,
		)
	}

	fs.ApplyChanges("test.fidl",
		[]state.Change{
			{
				Range: state.Range{
					Start: state.Position{Line: 1, Character: 16},
					End:   state.Position{Line: 2, Character: 19},
				},
				NewContent: "",
			},
		},
	)

	expFile = `
library example;
`
	file, err = fs.File("test.fidl")
	if err != nil {
		t.Error(err)
	}
	if file != expFile {
		t.Errorf(
			"file changes were not applied correctly. expected:\n%s\nfound:\n%s\n",
			expFile,
			file,
		)
	}
}

func TestFsApplyChangesOutsideFileRange(t *testing.T) {
	fs := state.NewFileSystem()

	fs.NewFile("test.fidl", `library example;`)

	err := fs.ApplyChanges("test.fidl",
		[]state.Change{
			{
				Range: state.Range{
					Start: state.Position{Line: -1, Character: -1},
					End:   state.Position{Line: 0, Character: 0},
				},
				NewContent: "inserted text",
			},
		},
	)
	if err == nil {
		t.Errorf("expect error on applying change out of file bounds")
	}

	err = fs.ApplyChanges("test.fidl",
		[]state.Change{
			{
				Range: state.Range{
					Start: state.Position{Line: 1, Character: 0},
					End:   state.Position{Line: 1, Character: 0},
				},
				NewContent: "inserted text",
			},
		},
	)
	if err == nil {
		t.Errorf("expect error on applying change out of file bounds")
	}
}
