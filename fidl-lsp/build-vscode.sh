#!/bin/bash

set -x

cd "$(dirname "$0")"
VSCODE_EXT_DIR=../vscode-language-fidl

# Fetch fidl{c, format} prebuilts from CIPD
cipd ensure -ensure-file sdk_ensure_file.txt -root sdk/

# Copy the prebuilts to the extension's bin/ directory
mkdir -p $VSCODE_EXT_DIR/bin/linux
cp sdk/linux/tools/x64/fidlc $VSCODE_EXT_DIR/bin/linux/fidlc
cp sdk/linux/tools/x64/fidl-format $VSCODE_EXT_DIR/bin/linux/fidl-format
mkdir -p $VSCODE_EXT_DIR/bin/mac
cp sdk/mac/tools/x64/fidlc $VSCODE_EXT_DIR/bin/mac/fidlc
cp sdk/mac/tools/x64/fidl-format $VSCODE_EXT_DIR/bin/mac/fidl-format

# Add write permissions to the files from the SDK so they can be updated in the
# future without sudo
chmod +w $VSCODE_EXT_DIR/bin/linux/fidlc
chmod +w $VSCODE_EXT_DIR/bin/linux/fidl-format
chmod +w $VSCODE_EXT_DIR/bin/mac/fidlc
chmod +w $VSCODE_EXT_DIR/bin/mac/fidl-format

# Cross-compile the language server into the extension's bin/ directory
env GOOS=linux GOARCH=amd64 go build -o $VSCODE_EXT_DIR/bin/linux/server main.go
env GOOS=darwin GOARCH=amd64 go build -o $VSCODE_EXT_DIR/bin/mac/server main.go

# Build the extension
npm --prefix $VSCODE_EXT_DIR install && npm --prefix $VSCODE_EXT_DIR run compile
