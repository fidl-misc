// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package langserver

import (
	"github.com/sourcegraph/go-lsp"

	"fidl-lsp/state"
)

func (h *LangHandler) handleOpenFile(params lsp.DidOpenTextDocumentParams) {
	h.fs.NewFile(state.FileID(params.TextDocument.URI), params.TextDocument.Text)
}

func (h *LangHandler) handleCloseFile(params lsp.DidCloseTextDocumentParams) {
	h.fs.DeleteFile(state.FileID(params.TextDocument.URI))
}

func (h *LangHandler) handleDidChange(params lsp.DidChangeTextDocumentParams) {
	// Convert from []lsp.TextDocumentContentChangeEvent --> []state.Change
	changes := make([]state.Change, len(params.ContentChanges))
	for i, lspChange := range params.ContentChanges {
		changes[i] = state.Change{
			Range: state.Range{
				Start: state.Position{
					Line:      lspChange.Range.Start.Line,
					Character: lspChange.Range.Start.Character,
				},
				End: state.Position{
					Line:      lspChange.Range.End.Line,
					Character: lspChange.Range.End.Character,
				},
			},
			NewContent: lspChange.Text,
		}
	}

	if err := h.fs.ApplyChanges(state.FileID(params.TextDocument.URI), changes); err != nil {
		h.log.Printf("error on ApplyChanges: %s", err)
	}
}
