"use strict";

import * as path from "path";
import * as vscode from "vscode";
import * as lsp from "vscode-languageclient";
import FidlFormattingEditProvider from "./formatter";

let client: lsp.LanguageClient;

export async function activate(context: vscode.ExtensionContext) {
  if (!vscode.workspace.getConfiguration("fidl").get("enableLSP")) {
    vscode.languages.registerDocumentFormattingEditProvider(
      { language: "fidl" },
      new FidlFormattingEditProvider()
    );
    return;
  }

  let platform = getPlatform();

  if (platform === undefined) {
    vscode.window.showErrorMessage(
      "Unfortunately we don't ship binaries for your platform yet. " +
        "You'll need to clone fidl-lsp and build the language server and " +
        "extension from source."
    );
    return;
  }

  let server: lsp.Executable = {
    command: context.asAbsolutePath(path.join("bin", `${platform}/server`)),
  };

  let clientOptions: lsp.LanguageClientOptions = {
    // Register the server for FIDL files
    documentSelector: [{ scheme: "file", language: "fidl" }],
    synchronize: {
      // Notify the server about file changes to '.clientrc files contained in the workspace
      fileEvents: vscode.workspace.createFileSystemWatcher("**/.clientrc"),
    },
  };

  client = new lsp.LanguageClient(
    "fidl",
    "FIDL Language Server",
    server,
    clientOptions
  );

  // Start the client. This will also launch the server
  client.start();
}

export function deactivate(): Thenable<void> | undefined {
  if (!client) {
    return undefined;
  }
  return client.stop();
}

function getPlatform(): string | undefined {
  if (process.arch === "x64") {
    if (process.platform === "linux") return "linux";
    if (process.platform === "darwin") return "mac";
  }
  return undefined;
}
