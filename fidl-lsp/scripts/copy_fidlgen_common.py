#!/usr/bin/env python3
# Copyright 2020 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""
copy fidlgen's fidl/compiler/backend/common/identifiers.go from fuchsia.git

This script copies the `identifiers.go` file from the fildgen `common` package
in fuchsia.git. This is used to share common FIDL library identifier code with
the fidlgens. It should be run on build to maintain freshness.

TODO: it would be better if we could just directly import the `common` package.

The script assumes that it is run from the fidl-lsp root
directory.
"""
import os
from pathlib import Path
from shutil import copyfile

FUCHSIA_DIR = os.getenv('FUCHSIA_DIR')
FIDLGEN_COMMON_PATH = 'tools/fidl/lib/fidlgen/identifiers.go'


def copy_fidlgen_common():
    project_root = path = Path.cwd()
    assert project_root.name == 'fidl-lsp', 'Script should be run from fidl-lsp root directory'
    Path(f'{project_root}/third_party/common').mkdir(exist_ok=True)
    copyfile(f'{FUCHSIA_DIR}/{FIDLGEN_COMMON_PATH}', f'{project_root}/third_party/common/identifiers.go')


if __name__ == '__main__':
    copy_fidlgen_common()
