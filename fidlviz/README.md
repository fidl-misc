# fidlviz

fidlviz is a tool for visualizing the FIDL wire format.

It is entirely client-side, implemented in `index.html`, `style.css`, and `script.js`.

Click the Help button in the app to learn how it works.

## Known issues

fidlviz has only been tested in Chrome. It requires `BigInt` support, so it does not work in Safari and other browsers that lack support.
