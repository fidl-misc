// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package analysis

type externalDecl struct {
	kind     string
	resource *bool `json:"resource,omitempty"`
}

type dependency struct {
	Name         string
	Declarations map[string]externalDecl
}

type location struct {
	Filename             string
	Line, Column, Length int
}

type Attribute struct {
	Name  string
	Value string
}

type TypeKind string

const (
	ArrayType      TypeKind = "array"
	VectorType              = "vector"
	StringType              = "string"
	HandleType              = "handle"
	RequestType             = "request"
	PrimitiveType           = "primitive"
	IdentifierType          = "identifier"
)

type typeCtor struct {
	Name          string
	Args          []typeCtor
	Nullable      bool
	Size          *constValue    `json:"maybe_size,omitempty"`
	HandleSubtype *HandleSubtype `json:"maybe_handle_subtype,omitempty"`
}

type constValue struct {
	Value string
}

type declType struct {
	Kind TypeKind

	// Array
	ElementType  *declType `json:"element_type,omitempty"`
	ElementCount uint      `json:"element_count,omitempty"`

	// Vector
	MaybeElementCount *uint `json:"maybe_element_count,omitempty"`
	Nullable          bool  `json:"nullable,omitempty"`

	// Handle
	Subtype string `json:"subtype,omitempty"`
	Rights  uint   `json:"rights,omitempty"`

	Identifier string `json:"identifier,omitempty"`
}

type member struct {
	Name          string
	Loc           location    `json:"location"`
	Type          declType    `json:"type,omitempty"`
	FromTypeAlias typeCtor    `json:"experimental_maybe_from_type_alias,omitempty"`
	Attrs         []Attribute `json:"maybe_attributes,omitempty"`
}

type method struct {
	Name          string
	Loc           location    `json:"location"`
	MaybeRequest  []member    `json:"maybe_request,omitempty"`
	MaybeResponse []member    `json:"maybe_response,omitempty"`
	Attrs         []Attribute `json:"maybe_attributes,omitempty"`
}

type bitsDecl struct {
	Name          string
	Loc           location `json:"location"`
	Type          declType
	FromTypeAlias typeCtor    `json:"experimental_maybe_from_type_alias,omitempty"`
	Attrs         []Attribute `json:"maybe_attributes,omitempty"`
	Members       []member
}

type enumDecl struct {
	Name string
	Loc  location `json:"location"`
	// For some reason, Bits have a full type in the JSON IR, while enums have
	// a string
	Type          string
	FromTypeAlias typeCtor    `json:"experimental_maybe_from_type_alias,omitempty"`
	Attrs         []Attribute `json:"maybe_attributes,omitempty"`
	Members       []member
}

type constDecl struct {
	Name          string
	Loc           location `json:"location"`
	Type          declType
	Value         constValue  `json:"value"`
	FromTypeAlias typeCtor    `json:"experimental_maybe_from_type_alias,omitempty"`
	Attrs         []Attribute `json:"maybe_attributes,omitempty"`
}

type protocolDecl struct {
	Name    string
	Loc     location `json:"location"`
	Methods []method
	Attrs   []Attribute `json:"maybe_attributes,omitempty"`
}

type serviceDecl struct {
	Name    string
	Loc     location `json:"location"`
	Members []member
	Attrs   []Attribute `json:"maybe_attributes,omitempty"`
}

type structDecl struct {
	Name      string
	Loc       location `json:"location"`
	Members   []member
	Attrs     []Attribute `json:"maybe_attributes,omitempty"`
	Anonymous bool
}

type tableDecl struct {
	Name    string
	Loc     location `json:"location"`
	Members []member
	Attrs   []Attribute `json:"maybe_attributes,omitempty"`
}

type unionDecl struct {
	Name    string
	Loc     location `json:"location"`
	Members []member
	Attrs   []Attribute `json:"maybe_attributes,omitempty"`
}

type typeAliasDecl struct {
	Name     string
	Loc      location    `json:"location"`
	TypeCtor typeCtor    `json:"partial_type_ctor"`
	Attrs    []Attribute `json:"maybe_attributes,omitempty"`
}

// TODO: rename to FidlJSONIR, or JSONLibrary?

// FidlLibrary is the type of the deserialized JSON IR of a FIDL library.
type FidlLibrary struct {
	Version        string
	Name           string
	Deps           []dependency      `json:"library_dependencies"`
	BitsDecls      []bitsDecl        `json:"bits_declarations"`
	ConstDecls     []constDecl       `json:"const_declarations"`
	EnumDecls      []enumDecl        `json:"enum_declarations"`
	ProtocolDecls  []protocolDecl    `json:"interface_declarations"`
	ServiceDecls   []serviceDecl     `json:"service_declarations"`
	StructDecls    []structDecl      `json:"struct_declarations"`
	TableDecls     []tableDecl       `json:"table_declarations"`
	UnionDecls     []unionDecl       `json:"union_declarations"`
	TypeAliasDecls []typeAliasDecl   `json:"type_alias_declarations"`
	Decls          map[string]string `json:"declarations"`
}
