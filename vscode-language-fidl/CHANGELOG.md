# v0.0.1

- Initial release with basic syntax highlighting support for FIDL.

# v0.0.3

- Offer a "FIDL: Go To Source" command to jump from generated bindings to FIDL source when working in the Fuchsia tree.

# v0.1.0

- FIDL2 syntax highlighting.

# v0.2.0

- FIDL formatting support
- Syntax highlighting improvements

# v0.3.0

- LSP Language Server support, including:
  - Go to definition
  - Find all references
  - Hover for type information
  - Document links

# v0.3.1

- Bug fixes
  - Don't crash when fail to read `fidl_project.json`
  - Correctly lookup library on `textDocument/references` request
- Improvements
  - Notify the user when the server fails to read `fidl_project.json`
  - Rename config setting for clarity

# v0.3.2

- Bug fix: correctly handle custom `fidl_project.json` setting

# v0.3.3

- Syntax highlighting improvements
  - strict, flexible, and resource modifiers
  - highlight the type of `const` values
  - accept `alias` instead of `using`

# v0.3.4

- Bug fix: include missing language server binaries with extension

# v0.3.5

- Bug fixes
  - Update language server for compatibility with updated FIDL JSON IR
  - Update out of date path to FIDL tools
  - Update third_party/fidlgen which was out of date with fuchsia.git

# v0.4.0

- Update to handle new syntax from [RFC-0050](https://fuchsia.dev/fuchsia-src/contribute/governance/rfcs/0050_syntax_revamp)

# v0.4.1

- Fixed several syntax highlighting bugs
- Implemented full support for [RFC-0086](https://fuchsia.dev/fuchsia-src/contribute/governance/rfcs/0086_rfc_0050_attributes)
- Implemented full support for [RFC-0087](https://fuchsia.dev/fuchsia-src/contribute/governance/rfcs/0087_fidl_method_syntax)

# v0.5.0

- Implemented full support for [RFC-00138](https://fuchsia.dev/fuchsia-src/contribute/governance/rfcs/0138_handling_unknown_interactions)
- Improved protocol and service highlighting (https://fxbug.dev/99302).
- Fixed alias and const LHS type highlighting (https://fxbug.dev/99302).
- Fixed a bug regarding the LSP server's interaction with the formatter (https://fxbug.dev/103606).
