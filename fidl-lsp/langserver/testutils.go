// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package langserver

import (
	"log"
	"os"

	"fidl-lsp/analysis"
	"fidl-lsp/state"
)

var (
	fuchsiaDir     = os.Getenv("FUCHSIA_DIR")
	toolsPath      = fuchsiaDir + "/out/default/host_x64/"
	fidlcPath      = toolsPath + "fidlc"
	fidlLintPath   = toolsPath + "fidl-lint"
	fidlFormatPath = toolsPath + "fidl-format"
)

type TestFile struct {
	Name string
	Text string
}

func NewHandlerWithFiles(files ...TestFile) *LangHandler {
	handler := NewLangHandler(
		NewDefaultConfig(""),
		log.New(os.Stderr, "[LSP Test] ", log.Lshortfile),
		analysis.NewAnalyzer(
			analysis.Config{
				BuildRootDir:   fuchsiaDir,
				FidlcPath:      fidlcPath,
				FidlLintPath:   fidlLintPath,
				FidlFormatPath: fidlFormatPath,
			},
		),
	)

	for _, file := range files {
		handler.fs.NewFile(state.FileID(file.Name), file.Text)
		handler.analyzer.Analyze(handler.fs, state.FileID(file.Name))
	}

	return handler
}
