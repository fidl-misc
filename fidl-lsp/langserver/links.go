// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package langserver

import (
	"fmt"

	"github.com/sourcegraph/go-lsp"

	"fidl-lsp/state"
)

func (h *LangHandler) handleDocumentLinks(params documentLinkParams) ([]documentLink, error) {
	file, err := h.fs.File(state.FileID(params.TextDocument.URI))
	if err != nil {
		return nil, fmt.Errorf("could not find file `%s`", params.TextDocument.URI)
	}
	imports := state.ParsePlatformImportsMatch(file)
	links := []documentLink{}
	for _, m := range imports {
		links = append(links, documentLink{
			Range: lsp.Range{
				Start: lsp.Position{
					Line:      m.Range.Start.Line,
					Character: m.Range.Start.Character,
				},
				End: lsp.Position{
					Line:      m.Range.End.Line,
					Character: m.Range.End.Character,
				},
			},
			Target:  lsp.DocumentURI(h.cfg.FormatRefURI(m.Lib)),
			Tooltip: fmt.Sprintf("Fuchsia SDK Docs for library %s", m.Lib.FullyQualifiedName()),
		})
	}

	return links, nil
}

// TODO: fxr/ --> fxrev.dev/, fxb/ --> fxbug.dev/?
