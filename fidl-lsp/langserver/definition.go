// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package langserver

import (
	"github.com/sourcegraph/go-lsp"

	"fidl-lsp/state"
)

func (h *LangHandler) handleDefinition(params lsp.TextDocumentPositionParams) ([]lsp.Location, error) {
	sym, err := h.fs.SymbolAtPos(
		state.FileID(params.TextDocument.URI),
		state.Position{Line: params.Position.Line, Character: params.Position.Character},
	)
	if err != nil {
		h.log.Printf(
			"could not find symbol at position `%#v` in document `%s`\n",
			params.Position,
			params.TextDocument.URI,
		)
		return nil, err
	}

	locs, err := h.analyzer.DefinitionOfSymbol(h.fs, sym)
	if err != nil {
		h.log.Printf("error on definition: %s\n", err)
		return nil, err
	}

	// Convert state.Locations --> lsp.Locations
	lspLocs := make([]lsp.Location, len(locs))
	for i, loc := range locs {
		lspLocs[i] = lsp.Location{
			URI: lsp.DocumentURI(loc.FileID),
			Range: lsp.Range{
				Start: lsp.Position{Line: loc.Range.Start.Line, Character: loc.Range.Start.Character},
				End:   lsp.Position{Line: loc.Range.End.Line, Character: loc.Range.End.Character},
			},
		}
	}
	return lspLocs, nil
}
