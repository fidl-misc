// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package langserver

import (
	"testing"

	"github.com/sourcegraph/go-lsp"
)

func TestFormat(t *testing.T) {
	handler := NewHandlerWithFiles(TestFile{
		Name: "test.fidl",
		Text: `
library example;
struct S { int32 field; };
protocol P {
UnindentedMethod  ( uint8 param1 , string param2 )  ;
};
table T   { 1  :uint u;
};
`,
	})

	edits, err := handler.handleFormat(lsp.DocumentFormattingParams{
		TextDocument: lsp.TextDocumentIdentifier{
			URI: lsp.DocumentURI("test.fidl"),
		},
	})
	if err != nil {
		t.Fatalf("failed to format file: %s", err)
	}

	if len(edits) != 1 {
		t.Errorf("incorrect number of formatting edits: expected 1, got %d", len(edits))
	}
	expRange := lsp.Range{
		Start: lsp.Position{Line: 0, Character: 0},
		End:   lsp.Position{Line: 14, Character: 0},
	}
	if edits[0].Range != expRange {
		t.Errorf(
			"formatted range does not span entire file: expected %v, got %v",
			expRange,
			edits[0].Range,
		)
	}

	expText := `library example;

struct S {
    int32 field;
};

protocol P {
    UnindentedMethod(uint8 param1, string param2);
};

table T {
    1: uint u;
};
`
	if edits[0].NewText != expText {
		t.Errorf("formatted text is incorrect: expected %s, got %s", expText, edits[0].NewText)
	}
}

func TestFormatFileWithErrors(t *testing.T) {
	handler := NewHandlerWithFiles(TestFile{
		Name: "test.fidl",
		Text: `library example`,
	})

	_, err := handler.handleFormat(lsp.DocumentFormattingParams{
		TextDocument: lsp.TextDocumentIdentifier{
			URI: lsp.DocumentURI("test.fidl"),
		},
	})

	if err == nil {
		t.Errorf("formatting file with errors did not fail")
	}
}

func TestFormatNonexistentFile(t *testing.T) {
	handler := NewHandlerWithFiles()

	_, err := handler.handleFormat(lsp.DocumentFormattingParams{
		TextDocument: lsp.TextDocumentIdentifier{
			URI: lsp.DocumentURI("test.fidl"),
		},
	})

	if err == nil {
		t.Errorf("formatting nonexistent file did not fail")
	}
}
