// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package fidlgen

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

func TestCodegenImplDotGo(t *testing.T) {
	path, err := os.Getwd()
	if err != nil {
		t.Fatalf("error getting cwd: %s", err)
	}
	identifiersDotGo, err := ioutil.ReadFile(fmt.Sprintf("%s/identifiers.go", path))
	if err != nil {
		t.Fatalf("error reading identifiers.go in fidl-lsp: %s", err)
	}
	fidlgenDir := fmt.Sprintf("%s/tools/fidl/lib/fidlgen", os.Getenv("FUCHSIA_DIR"))
	actualIdentifiersDotGo, err := ioutil.ReadFile(fmt.Sprintf("%s/identifiers.go", fidlgenDir))
	if err != nil {
		t.Fatalf("error reading identifiers.go in fuchsia.git: %s", err)
	}
	if bytes.Compare(identifiersDotGo, actualIdentifiersDotGo) != 0 {
		t.Fatalf("common/identifers.go is out of date from fuchsia.git, please run copy_fidlgen_common.py")
	}
}
